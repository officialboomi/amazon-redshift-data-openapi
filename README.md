# Amazon Redshift Data Connector
You can use the Amazon Redshift Data API to run queries on Amazon Redshift tables. You can run SQL statements, which are committed if the statement succeeds.

Documentation: https://docs.aws.amazon.com/redshift-data/latest/APIReference/Welcome.html

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/redshift-data/2019-12-20/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

